# 0.0.6
# local package
# -e .
# external requirements
click
coverage
flake8
GitPython
# backwards compatibility
pandas
pathlib
pathlib2
pycrypto
pytest-cov
python-dotenv>=0.5.1
Sphinx
tqdm
wheel
xml2dict
